<?php 
require_once 'layout.php';

session_start();

?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<div class="container"><br>
		<form action="funcoes.php" method="post">
			<div class="row">
				<div class="col-md-4">
					<h3>Criar Conta</h3>
					<hr /> 								
				</div>			
			</div>	
			<div class="row"> 
				<div class="col-md-4">
					<label>Valor de Saque</label>
					<input type="number" name="saldo" class="form-control" max="<?php echo $_SESSION['saldo']?>" required> 
				</div>
				<div class="col-md-4">						
					<input type="submit" value="Sacar" class="btn btn-success">
					<input type="hidden" name="tipo" value="sacar">
					<a href="index.php" class="btn btn-danger">Voltar</a>
				</div>		
			</div>
			<hr /> 		
		</form>	
	</div>
</body>
</html>