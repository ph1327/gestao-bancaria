<?php 
require_once 'layout.php';

session_start();

?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<div class="container"><br>
		<div class="row">
			<div class="col-md-4">
				<?php if (empty($_SESSION)){?>
				<h3>Criar Conta</h3>
				<?php } else{?>
					<h3><?php echo $_SESSION['nome'];?></h3>
				<?php } ?>
			</div>
			<div class="col-md-4">
				<?php if (empty($_SESSION)){?>
				<a href="criarConta.php" class="btn btn-success"><i class="large material-icons">group_add</i></a> 	
				<?php } ?>
			</div>
			<div class="col-md-4">
				<?php if (!empty($_SESSION)){?>
				Agência: <?php echo $_SESSION['agencia']?> Conta corrente: <?php echo $_SESSION['conta_corrente']?>
				<?php } ?>
			</div>
		</div>
		<table class="table table-hover table-bordered">
			<thead>
				<tr>
					<td style="text-align: center;">Realizar Deposito</td>
					<td style="text-align: center;">Consultar saldo</td>
					<td style="text-align: center;">Realizar Saque</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<?php if(!empty($_SESSION)){?>
					<td style="text-align: center;">
						<a href="realizarDeposito.php" class="btn btn-success"><i class="large material-icons">monetization_on</i></a> 
					</td>
					<td style="text-align: center;">
						<a href="saldo.php" class="btn btn-info"><i class="large material-icons">account_balance_wallet</i></a> 
					</td>
					<td style="text-align: center;">
						<a href="saque.php" class="btn btn-danger"><i class="large material-icons">money_off</i></a> 
					</td>
					<?php }?>
				</tr>
			</tbody>
		</table>
	</div>
</body>
</html>