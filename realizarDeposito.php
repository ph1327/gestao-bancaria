<?php 
require_once 'layout.php';

?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<div class="container"><br>
		<form action="funcoes.php" method="post">
			<div class="row">
				<div class="col-md-4">
					<h3>Criar Conta</h3>
					<hr /> 								
				</div>			
			</div>	
			<div class="row"> 
				<div class="col-md-4">
					<label>Valor de Depósito</label>
					<input type="number" name="saldo" class="form-control" min="1" required> 
				</div>
				<div class="col-md-4">						
					<input type="submit" value="Depositar" class="btn btn-success">
					<input type="hidden" name="tipo" value="depositar">
					<a href="index.php" class="btn btn-danger">Voltar</a>
				</div>		
			</div>
			<hr /> 		
		</form>	
	</div>
</body>
</html>