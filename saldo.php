<?php 
require_once 'layout.php';

session_start();

?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<div class="container"><br>
		<div class="row">
			<div class="col-md-4">
				<h3>Saldo disponível</h3>
			</div>
			<div class="col-md-4">
				<?php echo $_SESSION['saldo']; ?>
			</div>
			<a href="index.php" class="btn btn-danger">Voltar</a>
		</div>
	</div>
</body>
</html>