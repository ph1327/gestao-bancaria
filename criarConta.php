<?php 
require_once 'layout.php';

?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<div class="container"><br>
		<form action="funcoes.php" method="post">
			<div class="row">
				<div class="col-md-4">
					<h3>Criar Conta</h3>
					<hr /> 								
				</div>			
			</div>	
			<div class="row"> 
				<div class="col-md-4">
					<label>Nome</label>
					<input type="text" name="nome" class="form-control" required> 

				</div>
				<div class="col-md-4">
					<label>CPF</label>				
					<input type="text" name="cpf" class="form-control" required> 
				</div>
				<div class="col-md-4">
					<label>Sexo</label>
					<input type="text" name="sexo" class="form-control" required> 
				</div>
			</div>
			<hr /> 		
			<div class="row">
				<div class="col-md-4">
					<label>Data nascimento</label>
					<input type="date" name="data_nascimento" class="form-control" required> 
				</div>
				<div class="col-md-4">
					<label>Agência</label>
					<input type="text" name="agencia" class="form-control" required> 
				</div>
				<div class="col-md-4">
					<label>Conta corrente</label>
					<input type="text" name="conta_corrente" class="form-control" required> 
				</div>		
			</div>		
			<hr /> 				
			<div class="row">
				<div class="col-md-4">						
				</div>
				<div class="col-md-4">
					<label>Saldo</label>
					<input type="money" name="saldo" class="form-control" required> 	
				</div>
				<div class="col-md-4">						
					<input type="submit" value="Enviar" class="btn btn-success">
					<a href="index.php" class="btn btn-success">Voltar</a>
				</div>				
			</div>				
				<input type="hidden" name="tipo" value="criarconta">						
			</div>
		</form>	
	</div>
</body>
</html>